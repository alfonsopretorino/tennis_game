package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShoulBeIncreased() {
		
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Act
		player.incrementScore();
		
		//Assert
		assertEquals(1, player.getScore());
	}
	
	@Test
	public void scoreShoulNotBeIncreased() {
		
		//Arrange
		Player player = new Player("Federer", 0);

		//Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreShouldeBeLove() {
		
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("love", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldeBeFifteen() {
		
		//Arrange
		Player player = new Player("Federer", 1);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("fifteen", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldeBeThirty() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("thirty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldeBeForty() {
		
		//Arrange
		Player player = new Player("Federer", 3);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("forty", scoreAsString);
		
	}
	
	@Test
	public void scoreShouldeBeNullIfNegative() {
		
		//Arrange
		Player player = new Player("Federer", -1);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void scoreShouldeBeNullIfMoreThanThree() {
		
		//Arrange
		Player player = new Player("Federer", 4);
		
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
		
	}
	
	@Test
	public void shouldBeTie() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		Player player1 = new Player("Nadal", 2);
		
		//Act
		boolean tie = player1.isTieWith(player);
		
		//Assert
		assertTrue(tie);
		
	}
	
	@Test
	public void shouldNotBeTie() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		Player player1 = new Player("Nadal", 1);
		
		//Act
		boolean tie = player1.isTieWith(player);
		
		//Assert
		assertFalse(tie);
		
	}
	
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 3);

		//Act
		boolean outcome = player.hasAtLeastFortyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 2);

		//Act
		boolean outcome = player.hasAtLeastFortyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 2);

		//Act
		boolean outcome = player.hasLessThanFortyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 3);

		//Act
		boolean outcome = player.hasLessThanFortyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldhasMoreThanFourtyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 4);

		//Act
		boolean outcome = player.hasMoreThanFourtyPoints();
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldnothasMoreThanFourtyPoints() {
		
		//Arrange
		Player player = new Player("Federer", 3);

		//Act
		boolean outcome = player.hasMoreThanFourtyPoints();
		
		//Assert
		assertFalse(outcome);
		
	}
	
	
	
	@Test
	public void shouldhasOnePointAdvantageOn() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		Player player1 = new Player("Nadal", 1);

		//Act
		boolean outcome = player.hasOnePointAdvantageOn(player1);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldnothasOnePointAdvantageOn() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		Player player1 = new Player("Nadal", 3);

		//Act
		boolean outcome = player.hasOnePointAdvantageOn(player1);
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void shouldhasAtLeastTwoPointsAdvantageOn() {
		
		//Arrange
		Player player = new Player("Federer", 3);
		Player player1 = new Player("Nadal", 1);

		//Act
		boolean outcome = player.hasAtLeastTwoPointsAdvantageOn(player1);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void shouldnothasAtLeastTwoPointsAdvantageOn() {
		
		//Arrange
		Player player = new Player("Federer", 2);
		Player player1 = new Player("Nadal", 4);

		//Act
		boolean outcome = player.hasAtLeastTwoPointsAdvantageOn(player1);
		
		//Assert
		assertFalse(outcome);
		
	}
}

package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void GameFifteenThirty() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void GameFortyThirty() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Federer forty - Nadal thirty", status);
	}
	
	@Test
	public void GamePlayer1wins() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Federer wins", status);
	}

	@Test
	public void GameFifteenForty() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	@Test
	public void GameDuce() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Deuce", status);
	}
	
	@Test
	public void GameAdvantage1Player() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Advantage Federer", status);
	}
	
	@Test
	public void GameAPlayer2Wins() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Nadal wins", status);
	}
	
	@Test
	public void GameAdvantage2Player() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 
		 game.incrementPlayerScore(playerName1);
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Advantage Nadal", status);
	}
	
	
	@Test
	public void GameDuce44() throws Exception {
		
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName1);
		 
		 
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName2);
		 
		 
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName2);
		 game.incrementPlayerScore(playerName1);
		 game.incrementPlayerScore(playerName2);
		 
		 
		 String status = game.getGameStatus();
		 
		 //Assert
		 assertEquals("Deuce", status);
	}
	
	
}
